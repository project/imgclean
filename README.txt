Base64 Image Clean

This is a small module designed to do one thing: prevent base64 image data from
reaching your database. Why would you want this? two reasons. The first is
database size, since base64 image data can be quite large, bloating out tables.
The second is search API, since you don't really need base64 data to be
indexed, this prevents the search API from scanning through it.

Whenever a node is saved (either upon creation or on editing), the module scans
through the text fields and looks for base64 image data. The data is then
extracted, saved into a file (located in files/imgclean), then replaced with an
appropriate href.

Upon initial install, the module will scan through all of your nodes and scrub
out this data automatically, saving the images as above.

The module does not resize or edit the image in any way, nor does it change the
format. Currently it handles png, jpg, and gif. It doesn't touch SVGs right now
(though this may happen in a furture release).
